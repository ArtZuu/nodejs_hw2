/* eslint-disable linebreak-style */
const Joi = require('joi');

const validateRegistration = Joi.object({
  username: Joi.string().alphanum().min(3).max(50).required(),
  password: Joi.string().min(5).max(50).required(),
});

module.exports = {
  validateRegistration,
};
