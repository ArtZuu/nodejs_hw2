/* eslint-disable linebreak-style */
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    minlength: 3,
    maxlength: 50,
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 200,
  },
  createdDate: {
    type: Date,
  },
},
{
  collection: 'users',
});

const User = mongoose.model('User', userSchema);
module.exports = {
  User,
};
