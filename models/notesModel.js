/* eslint-disable linebreak-style */
const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    unique: false,
    minlength: 1,
    maxlength: 1000,
  },
  completed: {
    type: Boolean,
    require: true,
  },
  createdDate: {
    type: Date,
  },
},
{
  collection: 'notes',
});
module.exports = mongoose.model('Note', notesSchema);

