/* eslint-disable linebreak-style */
require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');

const app = express();
const port = process.env.PORT;

const authRouter = require('./routers/auth');
const userRouter = require('./routers/user');
const notesRouter = require('./routers/notes');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(function(req, res, next) {
  const jwtoken = req.headers['authorization'];
  if (!jwtoken) {
    return res.status(400).send({'message': 'Token not found'});
  };
  const token = jwtoken.split(' ').pop();
  JWT.verify(token, process.env.JWT_SECRET, (e, data) => {
    if (e) {
      return res.status(400).send({'message': 'JWT not valid'});
    };
    req.userInfo = data;
    next();
  });
});

app.use('/api/notes', notesRouter);
app.use('/api/users', userRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://test:tesT@cluster0.ka1gv.mongodb.net/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port);
};

start();
