/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {getInfo, deleteUser,
  changePassword} = require('../controllers/userController');

router.get('/me', getInfo);

router.delete('/me', deleteUser);

router.patch('/me', changePassword);

module.exports = router;
