/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {createNote, getUserNotes} = require('../controllers/notesController');
const {getUsersNoteById, updateNote, checkUncheckNote,
  deleteNote} = require('../controllers/notesByIdController');

router.post('/', createNote);

router.get('/', getUserNotes);

router.get('/:id', getUsersNoteById);

router.put('/:id', updateNote);

router.patch('/:id', checkUncheckNote);

router.delete('/:id', deleteNote);

module.exports = router;
