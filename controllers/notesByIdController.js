/* eslint-disable linebreak-style */
const Note = require('../models/notesModel');
const getUsersNoteById = async (req, res) => {
  const noteId = req.params.id;

  if (!await Note.exists({_id: noteId})) {
    return res.status(400).json({message: 'Note not found'});
  }

  const note = await Note.findOne({_id: noteId}, {__v: false});
  return res.status(200).json({note});
};

const updateNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const {text} = req.body;
    const note = await Note.findOne({_id: noteId});
    note.text = text;
    await note.save();
    res.status(200).json({message: 'Note has been updated'});
  } catch {
    return res.status(404).json({
      message: `A note with id '${id}' was not found`,
    });
  }
};

const checkUncheckNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.updateOne({_id: noteId}, {completed: !this.completed});
  res.status(200).json({message: 'Note has been checked/unchecked'});
};

const deleteNote = async (req, res) => {
  try {
    await Note.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Your note was successfully deleted!'});
  } catch {
    return res.status(404).json({
      message: `A note with id '${req.params.id}' was not found`,
    });
  }
};

module.exports = {
  getUsersNoteById,
  updateNote,
  checkUncheckNote,
  deleteNote,
};
