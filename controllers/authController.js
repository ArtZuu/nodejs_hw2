/* eslint-disable linebreak-style */
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');
const {validateRegistration} = require('../middlewares/authMiddleware');


const registerUser = async (req, res) => {
  const validationResult = await validateRegistration.validate(req.body);
  if (validationResult.error) {
    return res.status(400)
        .send({'message': `${validationResult.error.message}`});
  };
  const {username, password: plainTextPassword} = req.body;
  const createdUser = await User.findOne({username});
  if (createdUser) {
    return res.status(400)
        .send({'message': `User with username ${username} are alredy exists`});
  };
  const password = bcrypt.hashSync(plainTextPassword, 5);

  try {
    const user = await User.create({
      username,
      password,
      createdDate: new Date(Date.now()),
    });
    await user.save();
    res.status(200).send({'message': 'User has been saved'});
  } catch (e) {
    res.status(500).send({'message': 'Server error'});
  }
};

const loginUser = async (req, res, next) => {
  const {username, password} = req.body;
  const existedUser = await User.findOne({username});
  if (!username || !password) {
    return res.status(400)
        .send({'message': `username or password field are empty`});
  }
  if (!existedUser) {
    return res.status(400)
        .send({'message': `Username  ${username} not found`});
  }
  if (!await bcrypt.compare(password, existedUser.password)) {
    return res.status(400).send({'message': 'Wrong password'});
  };
  // eslint-disable-next-line camelcase
  const token = jwt
      .sign({_id: existedUser._id}, process.env.JWT_SECRET);
  return res.status(200).json({message: 'Success', jwt_token: token});
};

module.exports = {
  loginUser,
  registerUser,
};
