/* eslint-disable linebreak-style */
const {User} = require('../models/userModel');
const Note = require('../models/notesModel');
const bcrypt = require('bcrypt');

const getInfo = async (req, res) => {
  const _id = req.userInfo._id;
  const user = await User.findById(_id);
  const {username, createdDate} = user;

  res.status(200).json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
};

const deleteUser = async (req, res) => {
  const _id = req.userInfo._id;

  await User.findByIdAndDelete(_id);
  await Note.deleteMany({userId: _id});
  res.status(200).json({message: 'User has been deleted.'});
};

const changePassword = async (req, res) => {
  const _id = req.userInfo._id;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(_id);
  const isValidPassword = await bcrypt.compare(oldPassword, user.password);

  if (!isValidPassword) {
    return res.status(400).json({message: 'Invalid old password.'});
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
  res.status(200).json({message: 'Password has been changed.'});
};

module.exports = {
  getInfo,
  deleteUser,
  changePassword,
};
