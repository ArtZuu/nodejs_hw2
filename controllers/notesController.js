/* eslint-disable linebreak-style */
const Note = require('../models/notesModel');

const createNote = async (req, res) => {
  console.log(req.body.text);
  const userId = req.userInfo._id;
  const text = req.body.text;
  if (!text) {
    return res.status(400).json({message: `Please specify 'text' parameter`});
  }
  const note = new Note({
    userId,
    text,
    completed: false,
    createdDate: new Date(Date.now()),
  });

  await note.save();
  return res.status(200).json({message: 'Your note was successfully created!'});
};
const getUserNotes = async (req, res) => {
  const userId = req.userInfo._id;
  const {limit = 5, offset = 0} = req.query;
  const userNotes = await Note.find({userId}, {__v: 0}, {
    limit: +limit > 100 ? 5 : +limit,
    skip: +offset,
    sort: {
      createdDate: -1,
    },
  });

  res.status(200).json({
    notes: userNotes,
    limit,
    offset: +limit + +offset,
  });
};

module.exports = {
  createNote,
  getUserNotes,
};
